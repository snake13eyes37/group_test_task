import 'dart:async';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:group_test_task/src/core/singletones/environment/environments.dart';
import 'package:group_test_task/src/presentation/app/app_widget.dart';

void main() {

  runZonedGuarded(
        () async {
      WidgetsFlutterBinding.ensureInitialized();

      await Environment.init();

      Isolate.current.addErrorListener(
        RawReceivePort(
              (pair) async {
            final List<dynamic> errorAndStacktrace = pair;
            await Environment.recordError(
              errorAndStacktrace.first,
              errorAndStacktrace.last,
            );
          },
        ).sendPort,
      );
      if (Environment.isBlocLoggingEnabled) {
        Bloc.observer = Environment.blocObserver;
      }
      return runApp(const AppWidget());
    },
    Environment.recordError,
  );
}
