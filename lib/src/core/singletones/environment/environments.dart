import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:group_test_task/src/core/singletones/loggers/log.dart';
import 'package:group_test_task/src/core/singletones/loggers/loggers.dart';
import 'package:group_test_task/src/data/core/local/hive.dart';
import 'package:group_test_task/src/di/di.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:system_proxy/system_proxy.dart';




abstract class Environment {
  static const bool _isProd = !kDebugMode && !kProfileMode;

  static const bool isHttpLoggingEnabled = !_isProd;
  static const bool isBlocLoggingEnabled = !_isProd;
  static const bool canUseCustomUrl = !_isProd;

  static Log log = _isProd ? Log() : ConsolePrettyLog();

  static LoggingBlocObserver blocObserver = LoggingBlocObserver(log);
  static late PackageInfo packageInfo;

  static Future<void> init() async {
    WidgetsFlutterBinding.ensureInitialized();
    packageInfo = await PackageInfo.fromPlatform();
    await HiveConfig.init();
    Map<String, String?>? proxy = await SystemProxy.getProxySettings();

    initDI();

    // ignore: unawaited_futures
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    Bloc.observer = blocObserver;
  }

  static Future<void> recordFlutterError(
    FlutterErrorDetails flutterErrorDetails,
  ) async {
    log.e('RecordedFlutterError', flutterErrorDetails.exception,
        flutterErrorDetails.stack);

  }

  static Future<void> recordError(
    dynamic exception,
    StackTrace stack,
  ) async {
    log.e('RecordedError', exception, stack);
  }
}
