import 'package:logger/logger.dart';

class Log {
  /// Log a message at level verbose.
  void v(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level debug.
  void d(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level info.
  void i(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level warning.
  void w(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level error.
  void e(String message, [Object? error, StackTrace? stackTrace]) {}

  /// Log a message at level wtf.
  void wtf(String message, [Object? error, StackTrace? stackTrace]) {}
}

class ConsolePrettyLog implements Log {
  final logger = Logger(
    filter: ProductionFilter(),
    printer: PrettyPrinter(
      methodCount: 0,
      lineLength: 240,
      printEmojis: true,
    ),
  );

  @override
  void d(message, [error, StackTrace? stackTrace]) {
    logger.d(message, error: error, stackTrace: stackTrace);
  }

  @override
  void e(message, [error, StackTrace? stackTrace]) {
    logger.e(message, error: error, stackTrace: stackTrace);
  }

  @override
  void i(message, [error, StackTrace? stackTrace]) {
    logger.i(message, error: error, stackTrace: stackTrace);
  }

  @override
  void v(message, [error, StackTrace? stackTrace]) {
    logger.v(message, error: error, stackTrace: stackTrace);
  }

  @override
  void w(message, [error, StackTrace? stackTrace]) {
    logger.w(message, error: error, stackTrace: stackTrace);
  }

  @override
  void wtf(message, [error, StackTrace? stackTrace]) {
    logger.wtf(message, error: error, stackTrace: stackTrace);
  }
}
