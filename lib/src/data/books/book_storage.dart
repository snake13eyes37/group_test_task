import 'package:group_test_task/src/data/books/models/local/book_hive_dto.dart';
import 'package:group_test_task/src/data/core/local/hive.dart';
import 'package:hive/hive.dart';

class BookStorage {
  static const booksKey = 'books';

  Box<BookHiveDto>? _booksBox;

  Future<Box<BookHiveDto>> _getBox() async {
    return _booksBox ??= await HiveConfig.openBooksBox();
  }

  Future<List<BookHiveDto>?> loadBooksData() async {
    final box = await _getBox();
    return box.values.toList();
  }

  Future<void> saveBooksData(List<BookHiveDto> bookHiveDto)async{
    final box = await _getBox();
    await box.clear();
    await box.addAll(bookHiveDto);
    return;
  }

  Future<void> clear() => _getBox().then((box) => box.clear());
}
