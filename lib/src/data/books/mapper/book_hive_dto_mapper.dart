import 'package:group_test_task/src/data/books/models/local/book_hive_dto.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/core/core.dart';

final class BookHiveDtoMapper implements Mapper<BookModel, BookHiveDto> {
  @override
  BookModel call(BookHiveDto data) {
    return BookModel(
      id: data.id,
      title: data.title,
      author: data.author,
      year: data.year,
      isFavorite: data.isFavorite,
      image: data.image,
      description: data.description,
    );
  }

  BookHiveDto toDto(BookModel data) {
    return BookHiveDto(
      id: data.id,
      title: data.title,
      author: data.author,
      year: data.year,
      isFavorite: data.isFavorite,
      image: data.image,
      description: data.description,
    );
  }
}
