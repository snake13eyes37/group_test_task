import 'package:group_test_task/src/data/books/mapper/book_hive_dto_mapper.dart';
import 'package:group_test_task/src/data/books/models/local/book_hive_dto.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/core/mapper.dart';

final class BookHiveListMapper
    implements Mapper<List<BookModel>, List<BookHiveDto>> {
  @override
  List<BookModel> call(List<BookHiveDto> data) {
    return data.map(BookHiveDtoMapper().call).toList();
  }
}
