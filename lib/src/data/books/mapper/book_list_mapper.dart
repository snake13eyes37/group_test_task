import 'package:group_test_task/src/data/books/mapper/book_response_mapper.dart';
import 'package:group_test_task/src/data/books/models/book_response.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/core/core.dart';

final class BookListMapper implements Mapper<List<BookModel>, List<BookResponse>>{
  @override
  List<BookModel> call(List<BookResponse> data) {
    return data.map(BookResponseMapper().call).toList();
  }

}