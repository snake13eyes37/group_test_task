import 'package:group_test_task/src/data/books/models/book_response.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/core/core.dart';

final class BookResponseMapper implements Mapper<BookModel, BookResponse> {
  @override
  BookModel call(BookResponse data) {
    return BookModel(
      id: data.id,
      title: data.title,
      author: data.author,
      year: data.year,
      isFavorite: data.isFavorite,
      image: data.image,
      description: data.description,
    );
  }
}
