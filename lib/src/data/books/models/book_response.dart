import 'package:json_annotation/json_annotation.dart';

part 'book_response.g.dart';

@JsonSerializable()
class BookResponse {
  final String id;
  final String title;
  final String author;
  final DateTime year;
  final bool isFavorite;
  final String image;
  final String description;

  const BookResponse({
    required this.id,
    required this.title,
    required this.author,
    required this.year,
    required this.isFavorite,
    required this.image,
    required this.description,
  });

  factory BookResponse.fromJson(Map<String, dynamic> json) =>
      _$BookResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BookResponseToJson(this);
}
