// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BookResponse _$BookResponseFromJson(Map<String, dynamic> json) => BookResponse(
      id: json['id'] as String,
      title: json['title'] as String,
      author: json['author'] as String,
      year: DateTime.parse(json['year'] as String),
      isFavorite: json['isFavorite'] as bool,
      image: json['image'] as String,
      description: json['description'] as String,
    );

Map<String, dynamic> _$BookResponseToJson(BookResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'author': instance.author,
      'year': instance.year.toIso8601String(),
      'isFavorite': instance.isFavorite,
      'image': instance.image,
      'description': instance.description,
    };
