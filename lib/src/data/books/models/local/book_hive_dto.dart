import 'package:group_test_task/src/data/core/local/hive.dart';
import 'package:hive/hive.dart';

part 'book_hive_dto.g.dart';

@HiveType(typeId: HiveTypeId.book)
class BookHiveDto {
  @HiveField(0)
  final String id;

  @HiveField(1)
  final String title;

  @HiveField(2)
  final String author;

  @HiveField(3)
  final DateTime year;

  @HiveField(4)
  final bool isFavorite;

  @HiveField(5)
  final String image;

  @HiveField(6)
  final String description;

  const BookHiveDto({
    required this.id,
    required this.title,
    required this.author,
    required this.year,
    this.isFavorite = true,
    required this.image,
    required this.description,
  });
}
