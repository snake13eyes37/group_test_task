// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_hive_dto.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BookHiveDtoAdapter extends TypeAdapter<BookHiveDto> {
  @override
  final int typeId = 1;

  @override
  BookHiveDto read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BookHiveDto(
      id: fields[0] as String,
      title: fields[1] as String,
      author: fields[2] as String,
      year: fields[3] as DateTime,
      isFavorite: fields[4] as bool,
      image: fields[5] as String,
      description: fields[6] as String,
    );
  }

  @override
  void write(BinaryWriter writer, BookHiveDto obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.author)
      ..writeByte(3)
      ..write(obj.year)
      ..writeByte(4)
      ..write(obj.isFavorite)
      ..writeByte(5)
      ..write(obj.image)
      ..writeByte(6)
      ..write(obj.description);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BookHiveDtoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
