import 'package:group_test_task/src/data/books/book_storage.dart';
import 'package:group_test_task/src/data/books/mapper/book_hive_dto_mapper.dart';
import 'package:group_test_task/src/data/books/mapper/book_hive_list_dto_mapper.dart';
import 'package:group_test_task/src/data/books/mapper/book_list_mapper.dart';
import 'package:group_test_task/src/data/books/service.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/books/repository/book_repository.dart';
import 'package:group_test_task/src/domain/core/loading_result.dart';

final class BookRepositoryImpl implements BooksRepository {
  final BookService _service;
  final BookStorage _storage;

  BookRepositoryImpl(this._service, this._storage);

  @override
  Future<LoadingResult<List<BookModel>>> getBooks() async {
    return executor(
      _service.getBooks(),
      BookListMapper(),
    );
  }

  @override
  Future<LoadingResult<void>> addFavoriteBook(BookModel bookModel) async {
    final books = await _storage.loadBooksData();
    books?.add(BookHiveDtoMapper().toDto(bookModel));
    return executor(
      _storage.saveBooksData(books ?? []),
      null,
    );
  }

  @override
  Future<LoadingResult<void>> deleteFavoriteBook(BookModel bookModel) async {
    final books = await _storage.loadBooksData();
    books?.removeWhere((element) => element.id == bookModel.id);
    return executor(
      _storage.saveBooksData(books ?? []),
      null,
    );
  }

  @override
  Future<LoadingResult<List<BookModel>>> getFavoriteBooks() {
    return executor(
      _storage.loadBooksData(),
      BookHiveListMapper(),
    );
  }
}
