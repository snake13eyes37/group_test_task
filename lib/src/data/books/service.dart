import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:group_test_task/src/data/books/models/book_response.dart';

const _jsonPath = "assets/json/books.json";

abstract interface class BookService {
  Future<List<BookResponse>> getBooks();
}

final class BookServiceImpl implements BookService {
  @override
  Future<List<BookResponse>> getBooks() {
    return rootBundle.loadString(_jsonPath).then((value) {
      List data = json.decode(value);
      return data.map((e)=>BookResponse.fromJson(e as Map<String, dynamic>)).toList();
    });
  }
}
