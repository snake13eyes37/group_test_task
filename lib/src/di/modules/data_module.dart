import 'package:get_it/get_it.dart';
import 'package:group_test_task/src/data/books/book_storage.dart';
import 'package:group_test_task/src/data/books/repository/book_repository_impl.dart';
import 'package:group_test_task/src/data/books/service.dart';
import 'package:group_test_task/src/domain/books/repository/book_repository.dart';

void registerDataModule(GetIt container) {
  _registerBooks(container);
}

void _registerBooks(GetIt container) {
  container.registerFactory<BookStorage>(
    () => BookStorage(),
  );

  container.registerFactory<BookService>(
    () => BookServiceImpl(),
  );

  container.registerFactory<BooksRepository>(
    () => BookRepositoryImpl(
      container.get(),
      container.get(),
    ),
  );
}
