import 'package:get_it/get_it.dart';
import 'package:group_test_task/src/domain/books/use_case/add_to_favorite_use_case.dart';
import 'package:group_test_task/src/domain/books/use_case/favorite_action_use_case.dart';
import 'package:group_test_task/src/domain/books/use_case/get_books_use_case.dart';
import 'package:group_test_task/src/domain/books/use_case/get_favorite_books.dart';
import 'package:group_test_task/src/domain/books/use_case/remove_favorite_use_case.dart';

void registerDomainModule(GetIt container) {
  _registerBooks(container);
}

void _registerBooks(GetIt container) {
  container.registerFactory<GetBooksUseCase>(
    () => GetBooksUseCase(container.get()),
  );

  container.registerFactory<AddToFavoriteUseCase>(
    () => AddToFavoriteUseCase(container.get()),
  );

  container.registerFactory<RemoveFavoriteUseCase>(
    () => RemoveFavoriteUseCase(container.get()),
  );

  container.registerFactory<GetFavoriteBooksUseCase>(
    () => GetFavoriteBooksUseCase(container.get()),
  );

  container.registerFactory<FavoriteActionUseCase>(
    () => FavoriteActionUseCase(
      container.get(),
      container.get(),
    ),
  );
}
