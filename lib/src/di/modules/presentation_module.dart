import 'package:get_it/get_it.dart';
import 'package:group_test_task/src/presentation/book_detail_screen/bloc/book_detail_bloc.dart';
import 'package:group_test_task/src/presentation/home_screen/bloc/home_bloc.dart';
import 'package:group_test_task/src/presentation/splash_screen/bloc/splash_bloc.dart';

void registerPresentationModule(GetIt container) {
  _registerBloc(container);
}

void _registerBloc(GetIt container) {
  container.registerFactory<SplashScreenBloc>(
    () => SplashScreenBloc(),
  );

  container.registerFactory<HomeBloc>(
    () => HomeBloc(
      container.get(),
      container.get(),
      container.get(),
    ),
  );

  container.registerFactory<BookDetailBloc>(
    () => BookDetailBloc(
      container.get(),
    ),
  );
}
