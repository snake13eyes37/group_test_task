import 'package:get_it/get_it.dart';

import 'modules/modules.dart';

GetIt getIt = GetIt.instance;

void initDI() {
  registerDataModule(getIt);
  registerDomainModule(getIt);
  registerPresentationModule(getIt);
}
