final class BookModel{
  final String id;
  final String title;
  final String author;
  final DateTime year;
  final bool isFavorite;
  final String image;
  final String description;

//<editor-fold desc="Data Methods">
  const BookModel({
    required this.id,
    required this.title,
    required this.author,
    required this.year,
    required this.isFavorite,
    required this.image,
    required this.description,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is BookModel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          title == other.title &&
          author == other.author &&
          year == other.year &&
          isFavorite == other.isFavorite &&
          image == other.image &&
          description == other.description);

  @override
  int get hashCode =>
      id.hashCode ^
      title.hashCode ^
      author.hashCode ^
      year.hashCode ^
      isFavorite.hashCode ^
      image.hashCode ^
      description.hashCode;

  @override
  String toString() {
    return 'BookModel{' +
        ' id: $id,' +
        ' title: $title,' +
        ' author: $author,' +
        ' year: $year,' +
        ' isFavorite: $isFavorite,' +
        ' image: $image,' +
        ' description: $description,' +
        '}';
  }

  BookModel copyWith({
    String? id,
    String? title,
    String? author,
    DateTime? year,
    bool? isFavorite,
    String? image,
    String? description,
  }) {
    return BookModel(
      id: id ?? this.id,
      title: title ?? this.title,
      author: author ?? this.author,
      year: year ?? this.year,
      isFavorite: isFavorite ?? this.isFavorite,
      image: image ?? this.image,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'title': this.title,
      'author': this.author,
      'year': this.year,
      'isFavorite': this.isFavorite,
      'image': this.image,
      'description': this.description,
    };
  }

  factory BookModel.fromMap(Map<String, dynamic> map) {
    return BookModel(
      id: map['id'] as String,
      title: map['title'] as String,
      author: map['author'] as String,
      year: map['year'] as DateTime,
      isFavorite: map['isFavorite'] as bool,
      image: map['image'] as String,
      description: map['description'] as String,
    );
  }

//</editor-fold>
}