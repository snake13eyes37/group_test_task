import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/core/core.dart';

abstract interface class BooksRepository{
  Future<LoadingResult<List<BookModel>>> getBooks();
  Future<LoadingResult<void>> addFavoriteBook(BookModel bookModel);
  Future<LoadingResult<void>> deleteFavoriteBook(BookModel bookModel);
  Future<LoadingResult<List<BookModel>>> getFavoriteBooks();
}