import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/books/use_case/add_to_favorite_use_case.dart';
import 'package:group_test_task/src/domain/books/use_case/remove_favorite_use_case.dart';
import 'package:group_test_task/src/domain/core/core.dart';

final class FavoriteActionUseCase implements UseCase<BookModel, void> {
  final AddToFavoriteUseCase _addToFavoriteUseCase;
  final RemoveFavoriteUseCase _removeFavoriteUseCase;

  FavoriteActionUseCase(
    this._addToFavoriteUseCase,
    this._removeFavoriteUseCase,
  );

  @override
  Future<LoadingResult<void>> call(BookModel book) {
    if (book.isFavorite) {
      return _removeFavoriteUseCase.call(book);
    } else {
      return _addToFavoriteUseCase.call(book);
    }
  }
}
