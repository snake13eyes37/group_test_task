import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/books/repository/book_repository.dart';
import 'package:group_test_task/src/domain/core/core.dart';

final class GetFavoriteBooksUseCase implements UseCase<NoParams, List<BookModel>> {
  final BooksRepository _repository;

  GetFavoriteBooksUseCase(this._repository);

  @override
  Future<LoadingResult<List<BookModel>>> call(NoParams params) {
    return _repository.getFavoriteBooks();
  }
}