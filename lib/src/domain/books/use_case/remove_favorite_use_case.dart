
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/books/repository/book_repository.dart';
import 'package:group_test_task/src/domain/core/loading_result.dart';
import 'package:group_test_task/src/domain/core/use_case.dart';

final class RemoveFavoriteUseCase implements UseCase<BookModel, void> {
  final BooksRepository _repository;

  RemoveFavoriteUseCase(this._repository);

  @override
  Future<LoadingResult<void>> call(BookModel params) {
    return _repository.deleteFavoriteBook(params);
  }
}
