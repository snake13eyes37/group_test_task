import 'dart:async';

import 'mapper.dart';

sealed class LoadingResult<T> {
  LoadingResult._();

  factory LoadingResult.error(Exception exception) => Error(exception);

  factory LoadingResult.success(T? data) => Success(data);

  FutureOr<void> when<R>({
    required Function(T?) success,
    required Function(Exception) error,
  }) async {
    final payload = this;
    switch (payload) {
      case Success<T>():
        return await success(payload.data);
      case Error<T>():
        return await error(payload.exception);
    }
  }
}

class Error<T> extends LoadingResult<T> {
  final Exception exception;

  Error(this.exception) : super._();
}

class Success<T> extends LoadingResult<T> {
  final T? data;

  Success(this.data) : super._();
}

Future<LoadingResult<V>> executor<T, V>(
  Future<T> task,
  Mapper<V, T>? mapper,
) async {
  try {
    var result = await task;
    return LoadingResult.success(mapper?.call(result));
  } on Exception catch (e) {
    return LoadingResult.error(e);
  }
}
