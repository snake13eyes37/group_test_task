abstract  interface class Mapper<Domain, Data> {
  Domain call(Data data);
}
