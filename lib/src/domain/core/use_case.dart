import 'package:equatable/equatable.dart';

import 'loading_result.dart';

abstract interface class UseCase<RequestType, ReturnType> {
  Future<LoadingResult<ReturnType>> call(RequestType params);
}

class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}
