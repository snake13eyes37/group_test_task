import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:group_test_task/src/presentation/app/app.dart';
import 'package:group_test_task/src/presentation/app/navigations/app_router.dart';
import 'package:group_test_task/src/ui/l10n/generated/l10n.dart';

class AppWidget extends StatefulWidget {
   const AppWidget({Key? key}) : super(key: key);

  @override
  _AppWidgetState createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> with WidgetsBindingObserver {
  final _router = AppRouter();
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: _router.config(),
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      theme: AppTheme.mainTheme,
      supportedLocales: S.delegate.supportedLocales,
    );
  }
}
