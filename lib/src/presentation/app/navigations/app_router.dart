import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/presentation/book_detail_screen/book_detail_page.dart';
import 'package:group_test_task/src/presentation/favorite_screen/favorite_page.dart';
import 'package:group_test_task/src/presentation/home_screen/home_page.dart';
import 'package:group_test_task/src/presentation/main_screen/main_page.dart';
import 'package:group_test_task/src/presentation/profile_screen/profile_page.dart';
import 'package:group_test_task/src/presentation/search_screen/search_page.dart';
import 'package:group_test_task/src/presentation/splash_screen/splash_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: SplashRouteRoute.page, initial: true),
        AutoRoute(
          page: MainRoute.page,
          children: _mainRouteChildren,
        ),
        AutoRoute(page: BookDetailRoute.page),
      ];

  static final List<AutoRoute> _mainRouteChildren = [
    AutoRoute(page: HomeRoute.page),
    AutoRoute(page: SearchRoute.page),
    AutoRoute(page: FavoriteRoute.page),
    AutoRoute(page: ProfileRoute.page),
  ];
}
