import 'package:flutter/material.dart';
import 'package:group_test_task/src/ui/core/colors.dart';

class AppTheme {
  AppTheme._();

  static ThemeData mainTheme = ThemeData(
    primaryColor: AppColors.black,
    primaryColorLight: AppColors.white,
    scaffoldBackgroundColor: AppColors.white,
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        shape: MaterialStateProperty.all<OutlinedBorder>(const StadiumBorder()),
        foregroundColor: const MaterialStatePropertyAll<Color>(AppColors.white),
        backgroundColor: const MaterialStatePropertyAll<Color>(AppColors.black),
      ),
    ),
    outlinedButtonTheme: const OutlinedButtonThemeData(
      style: ButtonStyle(
        foregroundColor: MaterialStatePropertyAll<Color>(AppColors.black),
      ),
    ),
  );
}
