
import 'package:bloc/bloc.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/books/use_case/favorite_action_use_case.dart';
import 'package:group_test_task/src/presentation/core/bloc/bloc.dart';

part 'book_detail_event.dart';

part 'book_detail_state.dart';

class BookDetailBloc extends Bloc<BookDetailEvent, BookDetailState> {
  final FavoriteActionUseCase _actionUseCase;

  BookDetailBloc(this._actionUseCase) : super(BookDetailState()) {
    on<Init>(_onInit);
    on<FavoriteTap>(_onFavoriteTap);
  }

  void _onInit(Init event, Emitter<BookDetailState> emit) {
    emit(state.copyWith(bookModel: event.bookModel));
  }

  void _onFavoriteTap(FavoriteTap event, Emitter<BookDetailState> emit) async {
    emit(state.copyWith(isLoading: true));
    final bookModel = state.bookModel;
    if (bookModel != null) {
      final result = await _actionUseCase.call(bookModel);
      result.when(
        success: (data) {
          emit(
            state.copyWith(
              bookModel: bookModel.copyWith(isFavorite: !bookModel.isFavorite),
              isLoading: false,
              error: "",
            ),
          );
        },
        error: (exception) {
          emit(state.copyWith(isLoading: false, error: exception.toString()));
        },
      );
    }
  }
}
