part of 'book_detail_bloc.dart';

class BookDetailEvent extends BlocEvent {
  BookDetailEvent._();

  factory BookDetailEvent.init(BookModel bookModel) => Init(bookModel);

  factory BookDetailEvent.favoriteTap() => FavoriteTap();
}

class Init extends BookDetailEvent {
  final BookModel bookModel;

  Init(this.bookModel) : super._();
}

class FavoriteTap extends BookDetailEvent {
  FavoriteTap() : super._();
}
