part of 'book_detail_bloc.dart';

class BookDetailState extends BlocState {
  final BookModel? bookModel;
  final bool isLoading;
  final String error;

  BookDetailState({
    this.bookModel,
    this.isLoading = false,
    this.error = "",
  });

  BookDetailState copyWith({
    BookModel? bookModel,
    bool? isLoading,
    String? error,
  }) {
    return BookDetailState(
      bookModel: bookModel ?? this.bookModel,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [
        bookModel,
        isLoading,
        error,
      ];
}
