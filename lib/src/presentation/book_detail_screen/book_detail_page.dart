import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get_it/get_it.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/presentation/book_detail_screen/bloc/book_detail_bloc.dart';
import 'package:group_test_task/src/ui/core/core.dart';
import 'package:group_test_task/src/ui/l10n/generated/l10n.dart';
import 'package:group_test_task/src/ui/utils/extensions/value_global_methods.dart';
import 'package:group_test_task/src/ui/widgets/general_button_widget.dart';
import 'package:group_test_task/src/ui/widgets/general_network_image.dart';

BookDetailBloc _bloc(context) => BlocProvider.of<BookDetailBloc>(context);

@RoutePage()
class BookDetailPage extends StatelessWidget {
  final BookModel book;

  const BookDetailPage({super.key, required this.book});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
      GetIt.I.get<BookDetailBloc>()
        ..add(BookDetailEvent.init(book)),
      child: const _BookDetailScreen(),
    );
  }
}

class _BookDetailScreen extends StatefulWidget {
  const _BookDetailScreen({super.key});

  @override
  State<_BookDetailScreen> createState() => _BookDetailScreenState();
}

class _BookDetailScreenState extends State<_BookDetailScreen> {
  void _blocListener(BuildContext context, BookDetailState state) {
    if (state.error.isNotEmpty) {
      //todo show  error
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<BookDetailBloc, BookDetailState>(
      listener: _blocListener,
      builder: _buildScreen,
    );
  }

  Widget _buildScreen(BuildContext context, BookDetailState state) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(
            left: 16,
            right: 16,
            top: 16,
            bottom: 24,
          ),
          child: Column(
            children: [
              _buildAppBar(),
              const SizedBox(height: 16),
              _buildBookBody(state),
              const SizedBox(height: 24),
              _buildButton(context, state),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAppBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          S.current.book_detail_title,
          style: AppTextStyles.mulBold20,
        ),
        GestureDetector(
          onTap: (){
            context.router.pop();
          },
          child: SvgPicture.asset(
            AppIcons.close,
            width: 24,
            height: 24,
          ),
        ),
      ],
    );
  }

  Widget _buildBookBody(BookDetailState state) {
    final book = state.bookModel;
    if (book == null) {
      return const Center(child: CircularProgressIndicator());
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 160,
          width: 100,
          child: GeneralNetworkImage(
            url: book.image,
            fit: BoxFit.cover,
          ),
        ),
        const SizedBox(width: 16),
        Expanded(
          child: SizedBox(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 4),
                  child: Text(
                    book.title,
                    style: AppTextStyles.montSemiBold14,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  book.year.year.toString(),
                  style: AppTextStyles.montMedium12.copyWith(
                    color: AppColors.gray,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  book.author,
                  style: AppTextStyles.montBold12.copyWith(
                    color: AppColors.authorGray,
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  book.description,
                  maxLines: 5,
                  style: AppTextStyles.montMedium12,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildButton(BuildContext context, BookDetailState state) {
    final buttonText = valueOrFalse(state.bookModel?.isFavorite)
        ? S.current.remove_favorite
        : S.current.add_to_favorite;
    return GeneralButtonWidget(
      minWidth: double.infinity,
      onTap: () {
        _bloc(context).add(BookDetailEvent.favoriteTap());
      },
      isButtonDisabled: state.isLoading || state.bookModel == null,
      iconWidget: Row(
        children: [
          SvgPicture.asset(
            AppIcons.whiteFavorite,
            width: 24,
            height: 24,
          ),
          const SizedBox(width: 8),
          Text(
            buttonText,
            style: AppTextStyles.montSemiBold14.copyWith(
              color: AppColors.white,
            ),
          )
        ],
      ),
    );
  }
}
