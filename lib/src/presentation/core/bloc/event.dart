import 'package:equatable/equatable.dart';

abstract class BlocEvent extends Equatable {
  @override
  List<Object?> get props => const [];

  @override
  String toString() => '$runtimeType($props)';
}
