import 'package:equatable/equatable.dart';

class BlocState extends Equatable {

  @override
  List<Object?> get props => [hashCode];

  @override
  String toString() => '$runtimeType($props)';
}
