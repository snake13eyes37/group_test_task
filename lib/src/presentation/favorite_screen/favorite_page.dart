import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:group_test_task/src/ui/core/text_styles.dart';

@RoutePage()
class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const _FavoriteScreen();
  }
}

class _FavoriteScreen extends StatefulWidget {
  const _FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<_FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<_FavoriteScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Favorite",
        style: AppTextStyles.montBold24,
      ),
    );
  }
}
