
import 'package:bloc/bloc.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/domain/books/use_case/favorite_action_use_case.dart';
import 'package:group_test_task/src/domain/books/use_case/get_books_use_case.dart';
import 'package:group_test_task/src/domain/books/use_case/get_favorite_books.dart';
import 'package:group_test_task/src/domain/core/core.dart';
import 'package:group_test_task/src/presentation/core/bloc/bloc.dart';
import 'package:group_test_task/src/ui/utils/extensions/list_extensions.dart';
import 'package:group_test_task/src/ui/utils/extensions/value_global_methods.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final GetBooksUseCase _getBooksUseCase;
  final GetFavoriteBooksUseCase _getFavoriteBooksUseCase;
  final FavoriteActionUseCase _favoriteActionUseCase;

  HomeBloc(
    this._getBooksUseCase,
    this._getFavoriteBooksUseCase,
    this._favoriteActionUseCase,
  ) : super(HomeState()) {
    on<Init>(_onInit);
    on<FavoriteTap>(_onFavoriteTap);
  }

  void _onInit(Init event, Emitter<HomeState> emit) async {
    final result = await _getBooksUseCase.call(NoParams());
    final favoriteResult = await _getFavoriteBooksUseCase.call(NoParams());
    result.when(
      success: (data) {
        favoriteResult.when(
          success: (favoriteData) {
            final books = data
                ?.map(
                  (e) =>
                      _mapBookWithFavorite(e, valueOrEmptyList(favoriteData)),
                )
                .toList();
            emit(state.copyWith(books: books, isLoading: false, error: ""));
          },
          error: (exception) {
            emit(state.copyWith(error: exception.toString(), isLoading: false));
          },
        );
      },
      error: (exception) {
        emit(state.copyWith(error: exception.toString(), isLoading: false));
      },
    );
  }

  BookModel _mapBookWithFavorite(BookModel book, List<BookModel> favorites) {
    final containedElement = favorites
        .firstWhereOrNull((BookModel element) => book.id == element.id);
    return containedElement != null ? book.copyWith(isFavorite: true) : book;
  }

  void _onFavoriteTap(FavoriteTap event, Emitter<HomeState> emit) async {
    await _favoriteActionUseCase.call(event.book);
    final newList =
        state.books.map((e) => _mapFavoriteTap(e, event.book)).toList();
    emit(state.copyWith(books: newList));
  }

  BookModel _mapFavoriteTap(BookModel currentBook, BookModel tappedBook) {
    if (currentBook.id == tappedBook.id) {
      return currentBook.copyWith(isFavorite: !currentBook.isFavorite);
    } else {
      return currentBook;
    }
  }
}
