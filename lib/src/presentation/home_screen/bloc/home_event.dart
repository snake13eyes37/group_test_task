part of 'home_bloc.dart';

class HomeEvent extends BlocEvent {
  HomeEvent._();

  factory HomeEvent.init() => Init();

  factory HomeEvent.favoriteTap(BookModel book) => FavoriteTap(book);
}

class Init extends HomeEvent {
  Init() : super._();
}

class FavoriteTap extends HomeEvent {
  final BookModel book;

  FavoriteTap(this.book) : super._();
}
