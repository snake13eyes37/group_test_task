part of 'home_bloc.dart';

class HomeState extends BlocState {
  final List<BookModel> books;
  final bool isLoading;
  final String error;

  HomeState({
    this.books = const [],
    this.isLoading = true,
    this.error = "",
  });

  HomeState copyWith({
    List<BookModel>? books,
    bool? isLoading,
    String? error,
  }) {
    return HomeState(
      books: books ?? this.books,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
    );
  }

  @override
  List<Object> get props => [
        books,
        isLoading,
        error,
      ];
}
