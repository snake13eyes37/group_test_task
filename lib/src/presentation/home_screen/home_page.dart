import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:group_test_task/src/presentation/book_detail_screen/book_detail_page.dart';
import 'package:group_test_task/src/presentation/home_screen/bloc/home_bloc.dart';
import 'package:group_test_task/src/ui/core/core.dart';
import 'package:group_test_task/src/ui/l10n/generated/l10n.dart';
import 'package:group_test_task/src/ui/widgets/bottom_sheet_dialog.dart';
import 'package:group_test_task/src/ui/widgets/general_book_widget.dart';

HomeBloc _bloc(context) => BlocProvider.of<HomeBloc>(context);

@RoutePage()
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          GetIt.instance.get<HomeBloc>()..add(HomeEvent.init()),
      child: const _HomeScreen(),
    );
  }
}

class _HomeScreen extends StatefulWidget {
  const _HomeScreen({Key? key}) : super(key: key);

  @override
  State<_HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<_HomeScreen> {
  void _blocListener(BuildContext context, HomeState state) {
    if (state.error.isNotEmpty) {
      //todo - show error
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
      listener: _blocListener,
      builder: _buildScreen,
    );
  }

  Widget _buildScreen(BuildContext context, HomeState state) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const SizedBox(height: 40),
            _buildTitle(),
            const SizedBox(height: 16),
            Expanded(child: _buildList(context, state)),
          ],
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          S.current.recommend_title,
          style: AppTextStyles.mulBold20,
        ),
        Text(
          S.current.all_books,
          style: AppTextStyles.mulSemiBold12.copyWith(
            color: AppColors.gray,
          ),
        ),
      ],
    );
  }

  Widget _buildList(BuildContext context, HomeState state) {
    if (state.isLoading) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }
    return ListView.separated(
      padding: const EdgeInsets.only(bottom: 16),
      itemBuilder: (context, index) =>
          _buildItem(context: context, index: index, state: state),
      separatorBuilder: (context, index) => const SizedBox(height: 12),
      itemCount: state.books.length,
    );
  }

  Widget _buildItem({
    required BuildContext context,
    required int index,
    required HomeState state,
  }) {
    final book = state.books[index];

    return GeneralBookWidget(
      book: book,
      onTap: () async {
        await showBottomSheetDialog(
          context,
          BookDetailPage(book: book),
        );

        _bloc(context).add(HomeEvent.init());
      },
      onFavoriteTap: () {
        _bloc(context).add(HomeEvent.favoriteTap(book));
      },
    );
  }
}
