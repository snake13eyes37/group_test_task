import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:group_test_task/src/presentation/app/navigations/app_router.dart';
import 'package:group_test_task/src/ui/widgets/bottom_navigation/bottom_navigation_widget.dart';

@RoutePage()
class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const _MainScreen();
  }
}

class _MainScreen extends StatefulWidget {
  const _MainScreen({Key? key}) : super(key: key);

  @override
  State<_MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<_MainScreen> {
  @override
  Widget build(BuildContext context) {
    return AutoTabsScaffold(
      routes: const [
        HomeRoute(),
        SearchRoute(),
        FavoriteRoute(),
        ProfileRoute(),
      ],
      bottomNavigationBuilder: (context, tabRouter) => AppBottomNavigationBar(
        currentIndex: tabRouter.activeIndex,
        onTap: tabRouter.setActiveIndex,
      ),
    );
  }
}
