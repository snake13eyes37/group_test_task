import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:group_test_task/src/ui/core/core.dart';
import 'package:group_test_task/src/ui/l10n/generated/l10n.dart';
import 'package:group_test_task/src/ui/utils/input_formatters.dart';
import 'package:group_test_task/src/ui/widgets/general_network_image.dart';
import 'package:group_test_task/src/ui/widgets/general_text_field.dart';

@RoutePage()
class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const _ProfileScreen();
  }
}

class _ProfileScreen extends StatefulWidget {
  const _ProfileScreen({Key? key}) : super(key: key);

  @override
  State<_ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<_ProfileScreen> {
  final String _nameFromName = "nameFormName";
  final _nameFocusNode = FocusNode();
  final String _secondNameFromName = "secondNameFormName";
  final _secondNameFocusNode = FocusNode();
  final String _emailFromName = "emailFormName";
  final _emailFocusNode = FocusNode();
  final String _phoneFromName = "_phoneFormName";
  final _phoneFocusNode = FocusNode();
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: KeyboardDismissOnTap(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            children: [
              const SizedBox(height: 48),
              Text(
                S.current.profile_nav_title,
                style: AppTextStyles.mulBold20,
              ),
              const SizedBox(height: 16),
              _buildPhotoRow(),
              const SizedBox(height: 24),
              _buildNameField(),
              const SizedBox(height: 12),
              _buildSecondNameField(),
              const SizedBox(height: 12),
              _buildEmailField(),
              const SizedBox(height: 12),
              _buildPhoneField(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNameField() {
    return GeneralTextField(
      formName: _nameFromName,
      formKey: _formKey,
      focusNode: _nameFocusNode,
      hintText: S.current.name_title,
      enabled: true,
      keyboardType: TextInputType.name,
    );
  }

  Widget _buildSecondNameField() {
    return GeneralTextField(
      formName: _secondNameFromName,
      formKey: _formKey,
      focusNode: _secondNameFocusNode,
      hintText: S.current.second_name_title,
      enabled: true,
      keyboardType: TextInputType.name,
    );
  }

  Widget _buildEmailField() {
    return GeneralTextField(
      formName: _emailFromName,
      formKey: _formKey,
      focusNode: _emailFocusNode,
      hintText: S.current.email_title,
      enabled: true,
      keyboardType: TextInputType.emailAddress,
    );
  }

  Widget _buildPhoneField() {
    return GeneralTextField(
      formName: _phoneFromName,
      formKey: _formKey,
      focusNode: _phoneFocusNode,
      hintText: S.current.phone_title,
      enabled: true,
      keyboardType: TextInputType.phone,
      inputFormatters: [InputFormatters.phoneFormatter()],
    );
  }

  Widget _buildPhotoRow() {
    return Row(
      children: [
        SizedBox(
          width: 80,
          height: 80,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: const GeneralNetworkImage(
              url: "",
              fit: BoxFit.fill,
            ),
          ),
        ),
        const SizedBox(width: 24),
        SizedBox(
          width: 167,
          child: Text(
            S.current.profile_info_title,
            style: AppTextStyles.montMedium12.copyWith(
              color: AppColors.gray,
            ),
          ),
        ),
      ],
    );
  }
}
