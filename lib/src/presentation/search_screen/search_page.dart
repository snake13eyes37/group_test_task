import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:group_test_task/src/ui/core/core.dart';

@RoutePage()
class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const _SearchScreen();
  }
}

class _SearchScreen extends StatefulWidget {
  const _SearchScreen({Key? key}) : super(key: key);

  @override
  State<_SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<_SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Search",
        style: AppTextStyles.montBold24,
      ),
    );
  }
}
