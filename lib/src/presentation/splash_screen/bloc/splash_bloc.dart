import 'package:bloc/bloc.dart';
import 'package:group_test_task/src/presentation/core/bloc/bloc.dart';

part 'splash_event.dart';

part 'splash_state.dart';

class SplashScreenBloc extends Bloc<SplashScreenEvent, SplashScreenState> {
  SplashScreenBloc() : super(SplashScreenState()) {
    on<SplashScreenDidLoad>(init);
  }

  void init(
    SplashScreenEvent? value,
    Emitter<SplashScreenState> emit,
  ) async {
    await Future.delayed(const Duration(seconds: 3));
    emit(state.copyWith(isNavigate: true));
  }
}
