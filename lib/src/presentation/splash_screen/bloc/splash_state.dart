part of 'splash_bloc.dart';

class SplashScreenState extends BlocState {
  final bool isNavigate;

  SplashScreenState({
    this.isNavigate = false,
  });

  SplashScreenState copyWith({
    bool? isNavigate,
  }) {
    return SplashScreenState(
      isNavigate: isNavigate ?? this.isNavigate,
    );
  }

  @override
  List<Object> get props => [
        isNavigate,
      ];
}
