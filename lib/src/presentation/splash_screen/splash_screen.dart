import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:group_test_task/src/presentation/app/navigations/app_router.dart';
import 'package:group_test_task/src/presentation/splash_screen/bloc/splash_bloc.dart';
import 'package:group_test_task/src/ui/core/core.dart';
import 'package:group_test_task/src/ui/l10n/generated/l10n.dart';

SplashScreenBloc _bloc(context) => BlocProvider.of<SplashScreenBloc>(context);


@RoutePage()
class SplashScreenPage extends StatelessWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SplashScreenBloc>(
      create: (context) => GetIt.I.get<SplashScreenBloc>(),
      child: const _SplashScreen(),
    );
  }
}

class _SplashScreen extends StatefulWidget {
  const _SplashScreen({Key? key}) : super(key: key);

  @override
  State<_SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<_SplashScreen> {
  void _blocListener(BuildContext context, SplashScreenState state) {
    if (state.isNavigate) {
      context.router
          .pushAndPopUntil(const MainRoute(), predicate: (context) => false);
    }
  }

  @override
  void initState() {
    super.initState();
    _bloc(context).add(SplashScreenEvent.didLoad());
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SplashScreenBloc, SplashScreenState>(
      builder: _buildScreen,
      listener: _blocListener,
    );
  }

  Widget _buildScreen(BuildContext context, SplashScreenState state) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              S.current.logo,
              style: AppTextStyles.mulBold32,
            ),
            Image.asset(
              AppImages.splashLogo,
            ),
          ],
        ),
      ),
    );
  }
}
