import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color black = Color(0xFF282828);
  static const Color white = Color(0xFFFFFFFF);
  static const Color gray = Color(0xFF8C8C8C);
  static const Color lightGray = Color(0xFFF6F6F6);
  static const Color authorGray = Color(0xFF595858);


  static const Color shadowColor = Color(0xFF576079);
}
