class AppIcons {
  AppIcons._();

  static String _getSvgImage(String name) => 'assets/icons/$name.svg';

  static String close = _getSvgImage("close");
  static String whiteFavorite = _getSvgImage("white_favorite");


  static String homeNavActive = _getSvgImage("home_nav_active");
  static String homeNavInactive = _getSvgImage("home_nav_inactive");
  static String searchNav = _getSvgImage("search_nav");
  static String favoriteNav = _getSvgImage("favorite_nav");
  static String profileNavActive = _getSvgImage("profile_nav_active");
  static String profileNavInactive = _getSvgImage("profile_nav_inactive");
  static String favoriteUnlike = _getSvgImage("favorite_unlike");
  static String favoriteLike = _getSvgImage("favorite_like");
}
