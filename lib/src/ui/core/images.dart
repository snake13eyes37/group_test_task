///При добавление изображений обязательно конвертируем их в webp
///при помощи скрипта webp_convert.sh, лн находится в папке tools

class AppImages {
  AppImages._();

  static String _getImage(String name) => 'assets/images/$name.png';

  static String splashLogo = _getImage("splash_logo");
}
