import 'package:flutter/material.dart';

import 'core.dart';

class AppTextStyles {
  AppTextStyles._();

  static const _montserratFontFamily = "Montserrat";
  static const _mulishFontFamily = "Mulish";

  // Montserrat font family text styles

  static TextStyle montBold24 = const TextStyle(
    fontFamily: _montserratFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 24,
  );

  static TextStyle montBold12 = const TextStyle(
    fontFamily: _montserratFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 12,
  );


  static TextStyle montSemiBold14 = const TextStyle(
    fontFamily: _montserratFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 14,
  );

  static TextStyle montMedium16 = const TextStyle(
    fontFamily: _montserratFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 16,
  );

  static TextStyle montMedium12 = const TextStyle(
    fontFamily: _montserratFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 12,
  );

  static TextStyle montMedium10 = const TextStyle(
    fontFamily: _montserratFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 10,
  );

  // Mulish font family text styles

  static TextStyle mulBold32 = const TextStyle(
    fontFamily: _mulishFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 32,
  );

  static TextStyle mulBold24 = const TextStyle(
    fontFamily: _mulishFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 24,
  );

  static TextStyle mulBold20 = const TextStyle(
    fontFamily: _mulishFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 20,
  );

  static TextStyle mulSemiBold12 = const TextStyle(
    fontFamily: _mulishFontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 12,
  );
}
