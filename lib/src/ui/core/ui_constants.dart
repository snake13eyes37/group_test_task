import 'package:flutter/material.dart';

abstract class UIConstants {
  UIConstants._();

  static const defaultDuration = Duration(milliseconds: 300);
  static const Duration defaultAnimationDuration = Duration(milliseconds: 500);
  static const double navigationBarHeight = 72;

}
