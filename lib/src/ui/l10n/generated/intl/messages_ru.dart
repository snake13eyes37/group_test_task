// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "add_to_favorite":
            MessageLookupByLibrary.simpleMessage("Добавить в избранное"),
        "all_books": MessageLookupByLibrary.simpleMessage("Все книги"),
        "book_detail_title":
            MessageLookupByLibrary.simpleMessage("Выбор редакции"),
        "email_title": MessageLookupByLibrary.simpleMessage("Email"),
        "favorite_nav_title": MessageLookupByLibrary.simpleMessage("Избранное"),
        "home_nav_title": MessageLookupByLibrary.simpleMessage("Главная"),
        "logo": MessageLookupByLibrary.simpleMessage("Busy Reader"),
        "name_title": MessageLookupByLibrary.simpleMessage("Имя"),
        "phone_title": MessageLookupByLibrary.simpleMessage("Номер телефона"),
        "profile_info_title": MessageLookupByLibrary.simpleMessage(
            "Введите своё имя и при делании добавьте фото профиля"),
        "profile_nav_title": MessageLookupByLibrary.simpleMessage("Профиль"),
        "recommend_title": MessageLookupByLibrary.simpleMessage("Рекомендации"),
        "remove_favorite":
            MessageLookupByLibrary.simpleMessage("Убрать из избранного"),
        "search_nav_title": MessageLookupByLibrary.simpleMessage("Поиск"),
        "second_name_title": MessageLookupByLibrary.simpleMessage("Фамилия")
      };
}
