// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Главная`
  String get home_nav_title {
    return Intl.message(
      'Главная',
      name: 'home_nav_title',
      desc: '',
      args: [],
    );
  }

  /// `Поиск`
  String get search_nav_title {
    return Intl.message(
      'Поиск',
      name: 'search_nav_title',
      desc: '',
      args: [],
    );
  }

  /// `Избранное`
  String get favorite_nav_title {
    return Intl.message(
      'Избранное',
      name: 'favorite_nav_title',
      desc: '',
      args: [],
    );
  }

  /// `Профиль`
  String get profile_nav_title {
    return Intl.message(
      'Профиль',
      name: 'profile_nav_title',
      desc: '',
      args: [],
    );
  }

  /// `Имя`
  String get name_title {
    return Intl.message(
      'Имя',
      name: 'name_title',
      desc: '',
      args: [],
    );
  }

  /// `Фамилия`
  String get second_name_title {
    return Intl.message(
      'Фамилия',
      name: 'second_name_title',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email_title {
    return Intl.message(
      'Email',
      name: 'email_title',
      desc: '',
      args: [],
    );
  }

  /// `Номер телефона`
  String get phone_title {
    return Intl.message(
      'Номер телефона',
      name: 'phone_title',
      desc: '',
      args: [],
    );
  }

  /// `Введите своё имя и при делании добавьте фото профиля`
  String get profile_info_title {
    return Intl.message(
      'Введите своё имя и при делании добавьте фото профиля',
      name: 'profile_info_title',
      desc: '',
      args: [],
    );
  }

  /// `Рекомендации`
  String get recommend_title {
    return Intl.message(
      'Рекомендации',
      name: 'recommend_title',
      desc: '',
      args: [],
    );
  }

  /// `Все книги`
  String get all_books {
    return Intl.message(
      'Все книги',
      name: 'all_books',
      desc: '',
      args: [],
    );
  }

  /// `Выбор редакции`
  String get book_detail_title {
    return Intl.message(
      'Выбор редакции',
      name: 'book_detail_title',
      desc: '',
      args: [],
    );
  }

  /// `Добавить в избранное`
  String get add_to_favorite {
    return Intl.message(
      'Добавить в избранное',
      name: 'add_to_favorite',
      desc: '',
      args: [],
    );
  }

  /// `Убрать из избранного`
  String get remove_favorite {
    return Intl.message(
      'Убрать из избранного',
      name: 'remove_favorite',
      desc: '',
      args: [],
    );
  }

  /// `Busy Reader`
  String get logo {
    return Intl.message(
      'Busy Reader',
      name: 'logo',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
