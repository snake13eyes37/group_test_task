
String valueOrEmpty(String? value, {String defaultValue = ''}) =>
    value ?? defaultValue;

bool valueOrFalse(bool? value)=> value ?? false;

List<T> valueOrEmptyList<T>(List<T>? value)=> value ?? [];