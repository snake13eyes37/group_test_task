import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';


class InputFormatters {
  InputFormatters._();

  static MaskTextInputFormatter phoneFormatter({
        String? initialText,
      }) {
    return MaskTextInputFormatter(
      mask: "+# (###) ### - ## - ##",
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy,
      initialText: initialText,
    );
  }
}
