import 'package:group_test_task/src/ui/core/icons.dart';
import 'package:group_test_task/src/ui/l10n/generated/l10n.dart';

enum BottomNavigationTab { home, search, favorite, profile }

extension BottomNavigationTabExtension on BottomNavigationTab {
  int get tabIndex {
    switch (this) {
      case BottomNavigationTab.home:
        return 0;
      case BottomNavigationTab.search:
        return 1;
      case BottomNavigationTab.favorite:
        return 2;
      case BottomNavigationTab.profile:
        return 3;
    }
  }

  String get title {
    switch (this) {
      case BottomNavigationTab.home:
        return S.current.home_nav_title;
      case BottomNavigationTab.search:
        return S.current.search_nav_title;
      case BottomNavigationTab.favorite:
        return S.current.favorite_nav_title;
      case BottomNavigationTab.profile:
        return S.current.profile_nav_title;
    }
  }

  String getIconData(bool isSelected) {
    switch (this) {
      case BottomNavigationTab.home:
        return isSelected ? AppIcons.homeNavActive : AppIcons.homeNavInactive;
      case BottomNavigationTab.search:
        return AppIcons.searchNav;
      case BottomNavigationTab.favorite:
        return AppIcons.favoriteNav;
      case BottomNavigationTab.profile:
        return isSelected
            ? AppIcons.profileNavActive
            : AppIcons.profileNavInactive;
    }
  }
}

BottomNavigationTab getBottomNavigationTabFromIndex(int index) {
  switch (index) {
    case 0:
      return BottomNavigationTab.home;
    case 1:
      return BottomNavigationTab.search;
    case 2:
      return BottomNavigationTab.favorite;
    case 3:
      return BottomNavigationTab.profile;
  }
  return BottomNavigationTab.home;
}
