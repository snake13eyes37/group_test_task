import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:group_test_task/src/ui/core/core.dart';
import 'package:group_test_task/src/ui/widgets/bottom_navigation/bottom_navigation_helper.dart';

typedef BottomNavigationBarItemTap = Function(int);

class AppBottomNavigationBar extends StatelessWidget {
  AppBottomNavigationBar({
    Key? key,
    required this.currentIndex,
    required this.onTap,
  }) : super(key: key);

  final int currentIndex;
  final BottomNavigationBarItemTap onTap;

  Color _color(bool isSelected) =>
      isSelected ? AppColors.black : AppColors.gray;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: UIConstants.navigationBarHeight,
      decoration: BoxDecoration(
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: AppColors.shadowColor.withOpacity(0.15),
            blurRadius: 4,
            offset: const Offset(0, -2),
          ),
        ],
      ),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: _buildItems(context: context),
        ),
      ),
    );
  }

  List<Widget> _buildItems({required BuildContext context}) {
    return [
      _buildBarItem(context: context, tab: BottomNavigationTab.home),
      _buildBarItem(context: context, tab: BottomNavigationTab.search),
      _buildBarItem(context: context, tab: BottomNavigationTab.favorite),
      _buildBarItem(context: context, tab: BottomNavigationTab.profile),
    ];
  }

  Widget _buildBarItem({
    required BuildContext context,
    required BottomNavigationTab tab,
  }) {
    var isSelected = currentIndex == tab.tabIndex;
    return Expanded(
      child: Material(
        color: AppColors.white,
        child: InkWell(
          hoverColor: Colors.transparent,
          splashFactory: NoSplash.splashFactory,
          onTap: () {
            onTap(tab.tabIndex);
          },
          child: Padding(
            padding: EdgeInsets.zero,
            child: SizedBox(
              height: UIConstants.navigationBarHeight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    tab.getIconData(isSelected),
                    width: 24,
                    height: 24,
                  ),
                  Text(
                    tab.title,
                    style: AppTextStyles.montMedium10.copyWith(
                      color: _color(isSelected),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
