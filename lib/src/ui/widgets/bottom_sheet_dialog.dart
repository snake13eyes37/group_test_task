import 'package:flutter/material.dart';
import 'package:group_test_task/src/ui/core/core.dart';

Future<dynamic> showBottomSheetDialog(
  BuildContext context,
  Widget body, {
  double maxHeight = 316,
}) async {
  return await showModalBottomSheet(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20.0),
    ),
    context: context,
    builder: (_) => BottomSheetScreenDialog(
      body: body,
    ),
    constraints: BoxConstraints(maxHeight: maxHeight),
    isScrollControlled: true,
  );
}

class BottomSheetScreenDialog extends StatelessWidget {
  final Widget body;

  const BottomSheetScreenDialog({
    Key? key,
    required this.body,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: AppColors.white,
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: body,
      ),
    );
  }
}
