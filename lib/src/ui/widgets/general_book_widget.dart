import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:group_test_task/src/domain/books/models/book_model.dart';
import 'package:group_test_task/src/ui/core/colors.dart';
import 'package:group_test_task/src/ui/core/core.dart';
import 'package:group_test_task/src/ui/widgets/general_network_image.dart';

class GeneralBookWidget extends StatelessWidget {
  final BookModel book;
  final VoidCallback onTap;
  final VoidCallback onFavoriteTap;

  const GeneralBookWidget({
    super.key,
    required this.book,
    required this.onTap,
    required this.onFavoriteTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.lightGray,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.only(
            top: 16,
            bottom: 16,
            left: 16,
            right: 24,
          ),
          child: Row(
            children: [
              SizedBox(
                height: 78,
                width: 48,
                child: GeneralNetworkImage(
                  url: book.image,
                  fit: BoxFit.cover,
                  bgColor: AppColors.authorGray,
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildBookInfo(),
                    _buildFavoriteIcon(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildBookInfo() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 4),
            child:Text(
              book.title,
              style: AppTextStyles.montSemiBold14,
            ),
          ),
          const SizedBox(height: 4),
          Text(
            book.year.year.toString(),
            style: AppTextStyles.montMedium12.copyWith(
              color: AppColors.gray,
            ),
          ),
          const SizedBox(height: 4),
          Text(
            book.author,
            style: AppTextStyles.montBold12.copyWith(
              color: AppColors.authorGray,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFavoriteIcon() {
    final String icon =
        book.isFavorite ? AppIcons.favoriteLike : AppIcons.favoriteUnlike;

    return GestureDetector(
      onTap: (){
        onFavoriteTap();
      },
      child: SvgPicture.asset(
        icon,
        width: 24,
        height: 24,
      ),
    );
  }
}
