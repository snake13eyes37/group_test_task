import 'package:flutter/material.dart';
import 'package:group_test_task/src/ui/core/colors.dart';

class GeneralButtonWidget extends StatelessWidget {
  final String text;
  final VoidCallback? onTap;
  final double? height;
  final double? minWidth;
  final EdgeInsets margins;
  final double borderRadius;
  final bool isButtonDisabled;
  final EdgeInsetsGeometry padding;
  final Widget? iconWidget;
  final Color textColor;
  final Color backgroundColor;
  final TextStyle? textStyle;

  const GeneralButtonWidget({
    Key? key,
    this.text = '',
    this.onTap,
    this.height,
    this.margins = const EdgeInsets.all(0),
    this.minWidth,
    this.isButtonDisabled = false,
    this.borderRadius = 10,
    this.padding = const EdgeInsets.symmetric(vertical: 14),
    this.textStyle,
    this.iconWidget,
    this.textColor = AppColors.white,
    this.backgroundColor = AppColors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margins,
      child: _buildMaterialButton(context),
    );
  }

  Widget _buildMaterialButton(BuildContext context) {
    OutlinedBorder shape = const StadiumBorder();
    final radius = borderRadius;
    shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(radius),
    );
    final foregroundColor = isButtonDisabled ? AppColors.white : textColor;
    final bgColor = isButtonDisabled ? AppColors.black : backgroundColor;
    final buttonStyle = ButtonStyle(
      padding: MaterialStatePropertyAll<EdgeInsetsGeometry>(padding),
      foregroundColor: MaterialStatePropertyAll<Color>(foregroundColor),
      backgroundColor: MaterialStatePropertyAll<Color>(bgColor),
      shape: MaterialStateProperty.all(shape),
    );
    return SizedBox(
      height: height,
      width: minWidth,
      child: ElevatedButton(
        onPressed: isButtonDisabled ? null : onTap,
        style: buttonStyle,
        child: FittedBox(child: iconWidget ?? Text(text)),
      ),
    );
  }
}
