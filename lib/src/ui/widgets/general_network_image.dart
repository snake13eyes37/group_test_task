import 'package:flutter/material.dart';
import 'package:group_test_task/src/ui/core/colors.dart';

class GeneralNetworkImage extends StatelessWidget {
  final String url;
  final BoxFit? fit;
  final Color? bgColor;

  const GeneralNetworkImage({
    Key? key,
    required this.url,
    this.fit,
    this.bgColor = AppColors.gray,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url.isEmpty) return _buildErrorWidget();
    return Image.network(
      url,
      fit: fit,
      errorBuilder: (_, __, ___) {
        return _buildErrorWidget();
      },
    );
  }

  Container _buildErrorWidget() {
    return Container(color: bgColor);
  }
}
