import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:group_test_task/src/ui/core/colors.dart';
import 'package:group_test_task/src/ui/core/text_styles.dart';
import 'package:group_test_task/src/ui/utils/extensions/value_global_methods.dart';

class GeneralTextField extends StatefulWidget {
  final double? height;
  final AutovalidateMode autovalidateMode;
  final bool enabled;
  final bool autofocus;
  final TextInputType? keyboardType;
  final FormFieldValidator<String>? validator;
  final String? labelText;
  final TextStyle? textStyle;
  final String? hintText;
  final Color? hintColor;
  final String formName;
  final FocusNode focusNode;
  final GlobalKey<FormBuilderState>? formKey;
  final Function(String?)? onChanged;
  final List<TextInputFormatter> inputFormatters;
  final int? maxLength;
  final TextEditingController? controller;
  final int maxLines;
  final int minLines;
  final EdgeInsets? contentPadding;
  final String? initialValue;
  final String? errorText;

  GeneralTextField({
    Key? key,
    this.errorText,
    this.autovalidateMode = AutovalidateMode.disabled,
    this.textStyle,
    this.keyboardType,
    this.validator,
    this.labelText,
    this.hintText,
    this.onChanged,
    this.enabled = true,
    this.autofocus = false,
    this.inputFormatters = const [],
    this.maxLength,
    this.controller,
    this.maxLines = 1,
    this.minLines = 1,
    this.contentPadding,
    this.initialValue,
    this.height,
    this.hintColor = AppColors.gray,
    required this.formName,
    required this.formKey,
    required this.focusNode,
  }) : super(key: key);

  @override
  _GeneralTextFieldState createState() => _GeneralTextFieldState();
}

class _GeneralTextFieldState extends State<GeneralTextField> {
  TextEditingController editingController = TextEditingController();

  void listener() {
    setState(() {});
  }

  void initListener() {
    widget.focusNode.addListener(listener);
  }

  TextStyle get _textStyle {
    final textStyle = widget.textStyle ?? AppTextStyles.montMedium16;
    return textStyle.copyWith(color: AppColors.black);
  }

  bool get _isShowTopLabel =>
      widget.hintText != null &&
      (widget.focusNode.hasFocus || editingController.value.text.isNotEmpty);

  @override
  void initState() {
    initListener();
    editingController = widget.controller ?? TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    widget.focusNode.removeListener(listener);
    super.dispose();
  }

  void _onTextChange(String? text) {
    widget.onChanged?.call(text);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        widget.height != null
            ? SizedBox(
                height: widget.height,
                child: _buildBody(),
              )
            : _buildBody(),
        _buildLabelWidget(),
      ],
    );
  }

  EdgeInsets _getContentPadding() {
    final padding;
    if (_isShowTopLabel) {
      padding = const EdgeInsets.only(
        top: 25,
        bottom: 8,
        left: 16,
        right: 8,
      );
    } else {
      padding = const EdgeInsets.only(
        top: 17,
        bottom: 17,
        left: 16,
        right: 8,
      );
    }

    return widget.contentPadding ?? padding;
  }

  Widget _buildBody() {
    final theme = Theme.of(context);
    return GestureDetector(
      onTap: () {
        widget.focusNode.requestFocus();
      },
      child: FormBuilderTextField(
        textAlignVertical: TextAlignVertical.bottom,
        textCapitalization: TextCapitalization.none,
        keyboardAppearance: Brightness.light,
        autocorrect: false,
        autofocus: widget.autofocus,
        style: _textStyle,
        autovalidateMode: widget.autovalidateMode,
        initialValue: widget.initialValue,
        maxLines: widget.maxLines,
        minLines: widget.minLines,
        controller: editingController,
        inputFormatters: widget.inputFormatters,
        focusNode: widget.focusNode,
        name: widget.formName,
        onChanged: _onTextChange,
        maxLength: widget.maxLength,
        decoration: _buildInputDecoration(theme),
        cursorWidth: 1,
        validator: widget.validator,
        keyboardType: widget.keyboardType,
        enabled: widget.enabled,
        autofillHints: const [AutofillHints.oneTimeCode],
      ),
    );
  }

  InputDecoration _buildInputDecoration(ThemeData theme) {
    return InputDecoration(
      isDense: true,
      filled: true,
      counterText: '',
      contentPadding: _getContentPadding(),
      hintText:
          widget.focusNode.hasFocus ? null : valueOrEmpty(widget.hintText),
      hintStyle: AppTextStyles.montMedium16.copyWith(color: widget.hintColor),
      errorText: widget.errorText,
      alignLabelWithHint: true,
      errorMaxLines: 3,
      floatingLabelBehavior: FloatingLabelBehavior.never,
      border: _getBorder(),
      disabledBorder: _getBorder(),
      focusedErrorBorder: _getBorder(),
      focusedBorder: _getBorder(),
      enabledBorder: _getBorder(),
      errorBorder: _getBorder(),
    );
  }

  OutlineInputBorder _getBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Colors.transparent, width: 0),
    );
  }

  Widget _buildLabelWidget() {
    if (_isShowTopLabel) {
      return Padding(
        padding: const EdgeInsets.only(top: 8, left: 16),
        child: Text(
          valueOrEmpty(widget.hintText),
          style: AppTextStyles.montMedium10.copyWith(
            color: AppColors.gray,
          ),
        ),
      );
    }

    return const SizedBox.shrink();
  }
}
